var myImage = document.querySelector('img');

myImage.onclick = function() {
    var mySrc = myImage.getAttribute('src');
    if(mySrc === 'images/rinmaru-character-maker.jpg') {
      myImage.setAttribute ('src','images/character-customization-appearance.jpg');
    } else {
      myImage.setAttribute ('src','images/rinmaru-character-maker.jpg');
    }
}

var myButton = document.querySelector('button');
var myHeading = document.querySelector('h1');

function setUserName() {
    var myName = prompt('Please enter your name.');
    localStorage.setItem('name', myName);
    myHeading.textContent = 'Hello, ' + myName + ' to Character Customization';
}

if(!localStorage.getItem('name')) {
    setUserName();
} else {
    var storedName = localStorage.getItem('name');
    myHeading.textContent = 'Hello, ' + storedName + ' to Character Customization';
}

myButton.onclick = function() {
    setUserName();
}